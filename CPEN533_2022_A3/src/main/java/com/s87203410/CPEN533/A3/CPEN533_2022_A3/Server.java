package com.s87203410.CPEN533.A3.CPEN533_2022_A3;

import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.CRC32;

import com.google.protobuf.ByteString;
import com.s87203410.CPEN533.A3.CPEN533_2022_A3.Utilities;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;

public class Server {

	static Node node1 = new Node();

	public static void main(String[] args) throws Exception {

		DatagramSocket serverSocket = new DatagramSocket(8000);
		DatagramPacket incomingDataPacket = new DatagramPacket(new byte[1000], 1000);

		while (true) {
			serverSocket.receive(incomingDataPacket);
			System.out.println("The server is running");

			byte[] dataByeArray = Arrays.copyOfRange(incomingDataPacket.getData(), 0, incomingDataPacket.getLength());
			Msg clientMessage = Msg.parseFrom(dataByeArray);
			Long messageChecksum = clientMessage.getCheckSum();
			ByteString messageID = clientMessage.getMessageID();
			ByteString messagePayload = clientMessage.getPayload();
			System.out.println("responseChecksum" + messageChecksum);
			System.out.println(messageID);
			System.out.println(messagePayload);

			KVRequest kvRequest = KVRequest.parseFrom(messagePayload);
			int command = kvRequest.getCommand();
			ByteString key = kvRequest.getKey();
			ByteString value = kvRequest.getValue();
			int version = kvRequest.getVersion();
			System.out.println("Command " + command);
			System.out.println("Key " + key);
			System.out.println("Value " + value);
			System.out.println("version " + version);
			int errCode = 0;

			switch (command) {
			case 1:
				put();
				break;
			case 2:
				get();
				break;
			case 3:
				remove();
				break;
			case 4:
				shutdown();
				break;
			case 5:
				wipeout();
				break;
			case 6:
				isAlive();
				break;

			}

			KVResponse kvResponse = KVResponse.newBuilder().setErrCode(errCode).build();
			sendResponse(kvResponse, messageID);
		}

	}

	public static void put() {

	}

	public static void get() {

	}

	public static void remove() {

	}

	public static void shutdown() {

	}

	public static void wipeout() {

	}

	public static void isAlive() {

	}

	public static void sendResponse(KVResponse kvResponse, ByteString messageID) {

		ByteString payload = kvResponse.toByteString();
		byte[] payloadArr = payload.toByteArray();
		byte[] messageArr = messageID.toByteArray();
		byte[] responseByteArray = new byte[messageArr.length + payloadArr.length];
		System.arraycopy(messageArr, 0, responseByteArray, 0, messageArr.length);
		System.arraycopy(payloadArr, 0, responseByteArray, messageArr.length, payloadArr.length);

		CRC32 responseChecksum = new CRC32();
		responseChecksum.update(responseByteArray);
		Msg responseMsg = Msg.newBuilder().setMessageID(messageID).setPayload(payload)
				.setCheckSum(responseChecksum.getValue()).build();

	}

}