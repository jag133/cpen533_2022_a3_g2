package com.s87203410.CPEN533.A3.CPEN533_2022_A3;

import java.io.IOException;
import java.nio.*;
import java.util.*;
import java.util.zip.CRC32;

import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequestOrBuilder;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;


import java.net.*;

public class TestClient {
	private static final int MAXRETRIES = 4;
	private static int timeout = 100;

	public static void main(String[] args) throws IOException {

		InetAddress serverAddress = InetAddress.getByName("localhost");
		int servPort = 8000;

		DatagramSocket socket = new DatagramSocket();
		socket.setSoTimeout(timeout);

		byte[] randomID = new byte[2];
		Random randomByte = new Random();
		randomByte.nextBytes(randomID);

		// 16 byte Unique Identifier to match request and response
		ByteBuffer uniqueID = ByteBuffer.allocate(16);
		uniqueID.order(ByteOrder.LITTLE_ENDIAN);
		uniqueID.put(serverAddress.getAddress());
		uniqueID.putShort((short) servPort);
		uniqueID.put(randomID);
		uniqueID.putLong(System.nanoTime());

		byte[] requestUniqueID = uniqueID.array();
		
		KVRequest payloadObj = KVRequest.newBuilder().setCommand(1).setKey(ByteString.copyFrom(randomID))
				.setValue(ByteString.copyFrom(randomID)).setVersion(1).build();
		byte[] payload = payloadObj.toByteArray();

		byte[] requestArray = new byte[requestUniqueID.length + payload.length];
		System.arraycopy(requestUniqueID, 0, requestArray, 0, requestUniqueID.length);
		System.arraycopy(payload, 0, requestArray, requestUniqueID.length, payload.length);

		CRC32 crc = new CRC32();
		crc.update(requestArray);

		Msg requestMessage = Msg.newBuilder().setMessageID(ByteString.copyFrom(requestUniqueID))
				.setPayload(payloadObj.toByteString()).setCheckSum(crc.getValue()).build();

		byte[] bytesToSend = requestMessage.toByteArray();

		DatagramPacket requestPacket = new DatagramPacket(bytesToSend, bytesToSend.length, serverAddress, servPort);
		DatagramPacket responsePacket = new DatagramPacket(new byte[1000], 1000);

		int retryCounter = 0;
		boolean receivedResponse = false;
		do {
			try {
				socket.send(requestPacket);
				receivedResponse = true;
			} catch (IOException e) {

			}

			if (!receivedResponse) {
				retryCounter++;
				timeout = timeout * 2;
				socket.setSoTimeout(timeout);
				System.out.println("Timed out. Retry #" + retryCounter);
			}

		} while ((!receivedResponse) && (retryCounter <= MAXRETRIES));

		socket.close();

	}

}
