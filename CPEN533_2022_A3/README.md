##Student Details: 

Name: Jagadeesh Subramanian

Student ID: 87203410

Secret Code: 7CE0A6AA03FB7DB54B50552CE33071EF

##Execution Instructions: 

Download A2.jar from the repository 'CPEN533_2022_A2_87203410/CPEN533_2022_A2'

Run the jar file using below command.

java -jar A2.jar 35.86.157.60 43102 87203410

##Sample Output:

Timed out. Retry #1

Timed out. Retry #2

Timed out. Retry #3

Student ID: 87203410

Secret Code Length: 16

Secret Code: 7CE0A6AA03FB7DB54B50552CE33071EF